# language: pt
Funcionalidade: Alterar status

Cenário: Posso alterar meu status
Dado que estou na tela de Status
Quando selecionar um status listado
Então devo ver o status atual alterado

Cenário: Posso alterar meu status personalizando o texto
Dado que estou na tela de Status
Quando clicar no botão Adicionar novo status
E digitar um texto personalizando
E clicar no botão ok
Então devo ver o status atual alterado

Funcionalidade: Criação de um novo grupo

Cenário: Posso criar um novo grupo
Dado que estou na tela de Novo grupo
Quando selecionar os participantes
E clicar no botão avançar
E digitar um nome para o grupo
E clicar no botão confirmar
Então devo ver o grupo criado

Cenário: Devo visualizar uma mensagem de erro ao tentar criar um novo grupo sem nome
Dado que estou na tela de Novo grupo
Quando selecionar os participantes
E clicar no botão avançar
E clicar no botão confirmar
Então não devo ver o grupo criado