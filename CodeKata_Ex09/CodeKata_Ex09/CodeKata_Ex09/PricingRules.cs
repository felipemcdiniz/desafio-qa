﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeKata_Ex09
{
    public class PricingRules
    {
        public char item { get; set; }
        public int quantitySpecialPrice { get; set; }
        public int specialPrice { get; set; }
        public int price { get; set; }
    }
}
