﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeKata_Ex09
{
    class CheckOut
    {
        private List<char> _goods = new List<char>();
        private List<PricingRules> _pricingRules = new List<PricingRules>();
        private int _total = 0;

        public CheckOut(List<PricingRules> pricing_rules)
        {
            _pricingRules = pricing_rules;
        }

        public void Scan(char item)
        {
            _goods.Add(item);
            try
            {
                PricingRules pricingRule = _pricingRules.Find(x => x.item == item);
                _total += pricingRule.price;

                if (pricingRule.quantitySpecialPrice != 0)
                {
                    int quantityOccurrences = _goods.Where(x => x.Equals(pricingRule.item)).Count();
                    if (quantityOccurrences % pricingRule.quantitySpecialPrice == 0)
                        _total -= ((quantityOccurrences / pricingRule.quantitySpecialPrice) * (pricingRule.quantitySpecialPrice * pricingRule.price - pricingRule.specialPrice));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An unexpected error has been thrown. Error: " + ex.Message);
            }
        }


        public int Total()
        {
            return _total;
        }

    }
}
