﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeKata_Ex09
{
    class Program
    {
        static void Main(string[] args)
        {
            List<PricingRules> pricing_rules = new List<PricingRules>();

            PricingRules prodA = new PricingRules();
            prodA.item = 'A';
            prodA.price = 50;
            prodA.quantitySpecialPrice = 3;
            prodA.specialPrice = 130;
            pricing_rules.Add(prodA);

            PricingRules prodB = new PricingRules();
            prodB.item = 'B';
            prodB.price = 30;
            prodB.quantitySpecialPrice = 2;
            prodB.specialPrice = 45;
            pricing_rules.Add(prodB);

            PricingRules prodC = new PricingRules();
            prodC.item = 'C';
            prodC.price = 20;
            prodC.quantitySpecialPrice = 0;
            prodC.specialPrice = 0;
            pricing_rules.Add(prodC);

            PricingRules prodD = new PricingRules();
            prodD.item = 'D';
            prodD.price = 15;
            prodD.quantitySpecialPrice = 0;
            prodD.specialPrice = 0;
            pricing_rules.Add(prodD);

            CheckOut co = new CheckOut(pricing_rules);

            co.Scan('D');
            Console.WriteLine(co.Total().ToString());
            co.Scan('A');
            Console.WriteLine(co.Total().ToString());
            co.Scan('B');
            Console.WriteLine(co.Total().ToString());
            co.Scan('A');
            Console.WriteLine(co.Total().ToString());
            co.Scan('B');
            Console.WriteLine(co.Total().ToString());
            co.Scan('A');
            Console.WriteLine(co.Total().ToString());
            Console.ReadKey();
        }
    }
}
